##############################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
##############################################################################


from logging.handlers import RotatingFileHandler


LOGCONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': { 
        'standard': { 
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'standard',
            'filename': '/home/jas/jas/src/git/github/jasonalansmith/public/qualityops/logs/qops-application-boot.log',
            'maxBytes': 104857600,
            'backupCount': 365
            }
    },
    'loggers': {
        'qops': {
            'handlers': ['file'],
            'level': 'DEBUG'
        }
    }
}

#LOGCONFIG_QUEUE = ['qops']


# Statement for enabling the development environment
DEBUG = True
SQLALCHEMY_ECHO = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define the database - we are working with

DBCONF_POSTGRES_QOPS_BACKLOG = {
    'user': 'jas',
    'pw': 'Kill2Joy_',
    'db': 'qops_backlog',
    'host': 'localhost',
    'port': '5432',
}

DBCONF_POSTGRES_QOPSLOG = {
    'user': 'jas',
    'pw': 'Kill2Joy_',
    'db': 'qopslog',
    'host': 'localhost',
    'port': '5432',
}

SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % DBCONF_POSTGRES_QOPS_BACKLOG

QOPS_DB_URI = 'postgresql+psycopg2://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % DBCONF_POSTGRES_QOPSLOG

SQLALCHEMY_BINDS = {
    'qops': SQLALCHEMY_DATABASE_URI,
    'qopslog': QOPS_DB_URI,
}

SQLALCHEMY_TRACK_MODIFICATIONS = False

DATABASE_CONNECT_OPTIONS = {'echo': True}

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"
