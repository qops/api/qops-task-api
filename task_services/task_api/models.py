########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from qops_tablet import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Task(Base):
    __tablename__ = 'task'

    name = db.Column(db.String)
    description = db.Column(db.String)
    target_start_date = db.Column(db.String)
    actual_date_started = db.Column(db.String)
    target_complete_date = db.Column(db.String)
    actual_complete_date = db.Column(db.String)
    date_due = db.Column(db.String)
    priority_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)


class TaskPriority(Base):
    __tablename__ = 'task_priority'

    name = db.Column(db.String)
    usage = db.Column(db.String)
